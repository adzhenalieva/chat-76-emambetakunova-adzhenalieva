import {DATA_FAILURE, DATA_NEW_SUCCESS, DATA_REQUEST, SEND_MESSAGE_SUCCESS} from "./action";

const initialState = {
    messages: [],
    error: null,
    lastDatetime: null


};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case DATA_NEW_SUCCESS:
            let messages = [...state.messages, ...action.response];
            return {
                ...state,
                messages: messages,
                lastDatetime: action.lastDate
            };
        case DATA_REQUEST:
            return {
                ...state,
                error: null
            };
        case DATA_FAILURE:
            return {
                ...state,
                error: action.error
            };
        case SEND_MESSAGE_SUCCESS:
            return {
                ...state
            };
        default:
            return state;
    }
};
export default reducer;