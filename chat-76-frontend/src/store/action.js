import axios from '../axios-api';
import {NotificationManager} from "react-notifications";

export const DATA_REQUEST = 'DATA_REQUEST';
export const DATA_NEW_SUCCESS = 'DATA_NEW_SUCCESS';
export const DATA_FAILURE = 'DATA_FAILURE';
export const SEND_MESSAGE_SUCCESS = "SEND_MESSAGE_SUCCESS";

export const dataRequest = () => ({type: DATA_REQUEST});
export const dataNewSuccess = (response, lastDate) => ({type: DATA_NEW_SUCCESS, response, lastDate});
export const dataFailure = error => ({type: DATA_FAILURE, error});
export const sendMessageSuccess = () => ({type: SEND_MESSAGE_SUCCESS});

export const saveNewMessages = response => {
    return dispatch => {
        if (response.length === 0) {
            console.log('there is no new posts');
        } else {
            let last = response.length - 1;
            const newMessages = response.map(message => {
                return {
                    datetime: message.datetime,
                    author: message.author,
                    text: message.text,
                    id: message.id
                };
            });
            let lastDate = response[last].datetime;
            dispatch(dataNewSuccess(newMessages, lastDate));
        }
    }
};

export const fetchNewData = (lastDatetime) => {
    return dispatch => {
        dispatch(dataRequest());
        const urlParams = lastDatetime ? '?datetime=' + lastDatetime : '';
        axios.get('/messages' + urlParams).then(response => {
            dispatch(saveNewMessages(response.data));
        }, error => {
            dispatch(dataFailure(error));
        });
    }
};

export const createNotification = (type, message) => {
    return () => {
        switch (type) {
            case 'success':
                NotificationManager.success(message, 'Success');
                break;
            case 'error':
                NotificationManager.error(message, 'Error 400', 5000);
                break;
            default:
                break;
        }
    };
};

export const sendMessage = messageData => {
    return dispatch => {
        dispatch(dataRequest());
        axios.post('/messages', messageData).then(response => {
            dispatch(sendMessageSuccess());
            dispatch(createNotification('success', response.data.message))
        }, error => {
            const err = error.response.data.message;
            dispatch(dataFailure(err));
            dispatch(createNotification('error', err))
        });
    }
};