import React, {Component} from 'react';
import './SendPost.css';

class SendPost extends Component {
    render() {
        return (
            <div>
                <input type="text"  placeholder="Enter text" className="TextInput" name="text" onChange={this.props.onChange} value={this.props.value}
                       onFocus={this.props.onFocus}/>
                <input type="text"  placeholder="Enter author name" className="AuthorInput" name="author" onChange={this.props.onChange} value={this.props.valueAuthor}
                       onFocus={this.props.onFocus}/>
                <button className="Button" onClick={this.props.onClick}>Send</button>
            </div>
        );
    }
}

export default SendPost;