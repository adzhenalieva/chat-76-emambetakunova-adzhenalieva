import React, {Component} from 'react';
import './Post.css';

class Post extends Component {

    render() {
        return (
            <div className="Post">
                <span className="Author">{this.props.author} : </span>
                <span className="Text">{this.props.text}</span>
                <br />
                <span className="Time">{this.props.time}</span>
            </div>

        );
    }
}

export default Post;