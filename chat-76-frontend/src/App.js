import React, {Component} from 'react';
import {connect} from "react-redux";
import {NotificationContainer} from 'react-notifications';
import {fetchNewData, sendMessage} from "./store/action";

import Post from './components/Post/Post'
import SendPost from './components/SendPost/SendPost';

import './App.css';


class App extends Component {

    state = {
        text: '',
        author: '',
        intervalId: null
    };


    componentDidMount() {
        this.props.getNewData(this.props.lastDatetime);

        let intervalId = setInterval(() => {
            this.props.getNewData(this.props.lastDatetime);
        }, 5000);
        this.setState({intervalId});
    }


    saveMessage = (event) => {
        let name = event.target.name;
        this.setState({[name]: event.target.value});
    };


    changeValue = (event) => {
        let name = event.target.name;
        this.setState({[name]: ''});
    };

    sendMessage = () => {
        let data = {
            author: this.state.author,
            text: this.state.text
        };

        this.props.sendMessage(data);
        this.setState({text: '', author: ''})

    };

    getTime = (response) => {
        let datetime1 = response;
        let date = datetime1.split('T')[0];
        let minutes = datetime1.split('T')[1];
        minutes = minutes[0] + minutes[1] + minutes[2] + minutes[3] + minutes[4];
        return date + ' ' + minutes;
    };

    render() {

        return (
                <div className="App">
                    <SendPost value={this.state.text}
                              valueAuthor={this.state.author}
                              onChange={event => this.saveMessage(event)}
                              onClick={this.sendMessage}
                              onFocus={event => this.changeValue(event)}
                    />
                    {this.props.messages.map((post, id) => (
                        <Post
                            key={id}
                            time={this.getTime(post.datetime)}
                            author={post.author}
                            text={post.text}
                        />
                    ))}
                    <NotificationContainer/>
                </div>
        )
    }
}


const mapStateToProps = state => {
    return {
        messages: state.messages,
        error: state.error,
        lastDatetime: state.lastDatetime
    }

};

const mapDispatchToProps = dispatch => ({
    getNewData: lastDatetime => dispatch(fetchNewData(lastDatetime)),
    sendMessage: data => dispatch(sendMessage(data))

});

export default connect(mapStateToProps, mapDispatchToProps)(App);
