const express = require('express');
const fileDb = require('../fileDb');
const nanoid = require('nanoid');

const router = express.Router();

router.get('/', (req, res) => {
    if (req.query.datetime) {
        const date = new Date(req.query.datetime);
        if (isNaN(date.getDate())) {
            res.status(400).send('Incorrect date');
        }
        res.send(fileDb.getNewItems(req.query.datetime));
    } else {
        res.send(fileDb.getItems());
    }

});


router.post('/', (req, res) => {
    if (req.body.text === '' || req.body.author === "") {
        res.status(400).send({message: 'Enter message and author name'});
    } else {
        req.body.id = nanoid();
        req.body.datetime = new Date().toISOString();
        fileDb.addItem(req.body);
        res.send({message: "Message sent successfully"});
    }
});

module.exports = router;