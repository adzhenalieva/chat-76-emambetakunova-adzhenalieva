const express = require('express');
const messages = require('./app/messages');
const cors = require('cors');
const fileDb = require('./fileDb');
const app = express();
fileDb.init();


const port = 8000;
app.use(express.json());
app.use(cors());
app.use('/messages', messages);

app.listen(port, () => {
    console.log(`Server started on ${port} port`);
});